package renderer

import (
	"fmt"
	"image"
	"image/color"
	"log"
	"math"

	"bitbucket.org/zwzn/cis4800a2/object"
	"bitbucket.org/zwzn/parallel"
	pb "gopkg.in/cheggaaa/pb.v1"
)

func RenderObject(o *object.Object, width int, height int, camName string) (*image.RGBA, error) {
	r := image.Rect(0, 0, width, height)
	img := image.NewRGBA(r)
	faces := o.GetFaces()

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			img.SetRGBA(
				x,
				y,
				color.RGBA{255, 255, 255, 255},
			)
		}
	}

	cam := o.GetCamera(camName)
	// fmt.Printf("%v\n", cam)
	numFaces := len(faces)
	bar := pb.StartNew(numFaces)

	parallel.For(0, numFaces, 1, func(i, thread, total int) {
		face := faces[i]

		bar.Increment()

		m, err := cam.GetTransformations()
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		t, err := m.Multiply(face.Mesh.Transformation)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		a, err := face.A.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		b, err := face.B.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		c, err := face.C.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		pa, errA := project(a, cam, width, height)
		pb, errB := project(b, cam, width, height)
		pc, errC := project(c, cam, width, height)

		if errA == nil && errB == nil {
			drawLine(img, pa, pb, face.Colour)
		}
		if errC == nil && errB == nil {
			drawLine(img, pc, pb, face.Colour)
		}
		if errA == nil && errC == nil {
			drawLine(img, pa, pc, face.Colour)
		}
	})

	bar.FinishPrint("Finished render")
	fmt.Println()
	return img, nil
}

//start from https://stackoverflow.com/a/2049593
func sign(p1, p2, p3 *object.Vector2) float64 {
	return (p1.X-p3.X)*(p2.Y-p3.Y) - (p2.X-p3.X)*(p1.Y-p3.Y)
}

func pointInTriangle(vt, v1, v2, v3 *object.Vector2) bool {
	var b1, b2, b3 bool

	b1 = sign(vt, v1, v2) < 0.0
	b2 = sign(vt, v2, v3) < 0.0
	b3 = sign(vt, v3, v1) < 0.0

	return ((b1 == b2) && (b2 == b3))
}

// end from https://stackoverflow.com/a/2049593

var lastPer = -1

func loading(num, total, width int) {
	per := int(float32(num) / float32(total) * 100)
	if per != lastPer {
		fmt.Printf("\r%7s [", fmt.Sprintf("%d/%d", num, total))
		for i := 0; i < width; i++ {
			if float32(num)/float32(total) >= float32(i)/float32(width) {
				fmt.Print("=")
			} else {
				fmt.Print(".")
			}
		}
		fmt.Printf("] %d%%", per)
	}
}

func project(p *object.Vector3, cam *object.Camera, width int, height int) (*object.Vector2, error) {
	w := float64(width - 1)
	h := float64(height - 1)

	if p.Z < 0 || //p.Z > 1 ||
		p.X < -1 || p.X > 1 ||
		p.Y < -1 || p.Y > 1 {
		return nil, fmt.Errorf("The point was behind the camera")
	}

	// p2d := object.Vec2(
	// 	(p.X/p.Z)*cam.NearPlane,
	// 	(p.Y/p.Z)*cam.NearPlane,
	// )

	// fmt.Printf("%v %v\n", p.Angle(object.Vec3(0, 0, 1))/(2*math.Pi)*360, p2d)

	newP := object.Vec2(p.X, p.Y). //p2d.
					Add(object.Vec2(1, 1)).
					Dot(math.Min(w, h) / 2)
	return newP, nil
}

func drawLine(img *image.RGBA, p1 *object.Vector2, p2 *object.Vector2, colour color.RGBA) {
	if p1 == nil || p2 == nil {
		log.Fatalf("drawLine: One or both of the vectors are nil\n")
	}
	var sx, ex, sy, ey float64
	if math.Abs(p1.X-p2.X) > math.Abs(p1.Y-p2.Y) {
		if p1.X < p2.X {
			sx = p1.X
			sy = p1.Y
			ex = p2.X
			ey = p2.Y
		} else {
			sx = p2.X
			sy = p2.Y
			ex = p1.X
			ey = p1.Y
		}

		// fmt.Println("x", sx, sy, ex, ey)
		for x := sx; x <= ex; x++ {
			// println("test")
			y := sy + (x-sx)/(sx-ex)*(sy-ey)
			img.SetRGBA(
				int(x),
				int(y),
				colour,
			)
		}
	} else {
		if p1.Y < p2.Y {
			sx = p1.X
			sy = p1.Y
			ex = p2.X
			ey = p2.Y
		} else {
			sx = p2.X
			sy = p2.Y
			ex = p1.X
			ey = p1.Y
		}
		// fmt.Println("y", sx, sy, ex, ey)
		for y := sy + 1; y <= ey; y++ {

			x := sx + (y-sy)/(sy-ey)*(sx-ex)
			// fmt.Println(x, y)
			img.SetRGBA(
				int(x),
				int(y),
				colour,
			)
		}
	}
}

func drawPoint(img *image.RGBA, p *object.Vector2) {
	img.SetRGBA(
		int(p.X),
		int(p.Y),
		color.RGBA{0, 0, 0, 255},
	)
}
