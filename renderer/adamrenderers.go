package renderer

import (
	"fmt"
	"image"
	"image/color"
	"log"
	"math"

	"bitbucket.org/zwzn/cis4800a2/le"
	"bitbucket.org/zwzn/cis4800a2/object"
	"bitbucket.org/zwzn/parallel"
)

func RenderObject2(o *object.Object, width int, height int) (*image.RGBA, error) {
	r := image.Rect(0, 0, width, height)
	img := image.NewRGBA(r)
	faces := o.GetFaces()

	for x := 0; x < width; x++ {
		for y := 0; y < height; y++ {
			img.SetRGBA(
				x,
				y,
				color.RGBA{255, 255, 255, 255},
			)
		}
	}

	cam := o.GetCamera("")
	fmt.Printf("%v\n", cam)
	numFaces := len(faces)

	var dist [][]float64
	dist = make([][]float64, width)
	for x := 0; x < width; x++ {
		dist[x] = make([]float64, width)
		for y := 0; y < height; y++ {
			dist[x][y] = -1
		}
	}

	parallel.For(0, numFaces, 1, func(i, thread, total int) {
		face := faces[i]
		if thread == 0 {
			loading(i+1, numFaces, 40)
		}
		m, err := cam.GetTransformations()
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		t, err := m.Multiply(face.Mesh.Transformation)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		a, err := face.A.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		b, err := face.B.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}
		c, err := face.C.Transform(t)
		if err != nil {
			log.Fatalf("error in render: %v\n", err)
		}

		pa, _ := project(a, cam, width, height)
		pb, _ := project(b, cam, width, height)
		pc, _ := project(c, cam, width, height)

		minX, maxX := math.Min(pa.X, math.Min(pb.X, pc.X)), math.Max(pa.X, math.Max(pb.X, pc.X))
		minY, maxY := math.Min(pa.Y, math.Min(pb.Y, pc.Y)), math.Max(pa.Y, math.Max(pb.Y, pc.Y))

		for x := minX; x <= maxX; x++ {
			for y := minY; y <= maxY; y++ {
				pos := object.Vec2(x, y)
				if pointInTriangle(pos, pa, pb, pc) {

					p1, p2, p3 := le.Vec3(a.X, a.Y, a.Z), le.Vec3(b.X, b.Y, b.Z), le.Vec3(c.X, c.Y, c.Z)
					pl := le.NewPlaneFromPoints(p1, p2, p3)
					li := le.NewLineFromPoints(le.Zero3, le.Vec3(x/float64(width)*2-1, y/float64(height)*2-1, cam.NearPlane))
					// fmt.Println(li)
					po := pl.Intersection(li)
					if po != nil {
						v1 := object.Vec3(po.X, po.Y, po.Z)
						d := object.Zero3.Distance(v1)
						// fmt.Println(d)
						if dist[int(x)][int(y)] < d {
							dist[int(x)][int(y)] = d

							n := face.Normal(v1)

							// a := n.Inner(object.Vec3(1, 1, 0))
							if n.X != 0 || n.Y != 0 || n.Z != 1 {
								fmt.Println(n)
							}

							img.SetRGBA(
								int(x),
								int(y),
								color.RGBA{uint8((n.X + 1) * 125), uint8((n.Z + 1) * 125), uint8((n.Y + 1) * 125), 255},
							)
						}
					}
				}
			}
		}

		// drawLine(img, pa, pb, face.Colour)
		// drawLine(img, pc, pb, face.Colour)
		// drawLine(img, pa, pc, face.Colour)
	})

	fmt.Println()
	return img, nil
}
