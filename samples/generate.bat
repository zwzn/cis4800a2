cis4800a2 -camera a1 -if .\samples\xobjs\cube.xobj -of .\samples\imgs\a1-cube.png
cis4800a2 -camera a2 -if .\samples\xobjs\cube.xobj -of .\samples\imgs\a2-cube.png

cis4800a2 -camera a1 -if .\samples\xobjs\cylnder.xobj -of .\samples\imgs\a1-cylnder.png
cis4800a2 -camera a2 -if .\samples\xobjs\cylnder.xobj -of .\samples\imgs\a2-cylnder.png

cis4800a2 -camera a1 -if .\samples\xobjs\sphere.xobj -of .\samples\imgs\a1-sphere.png
cis4800a2 -camera a2 -if .\samples\xobjs\sphere.xobj -of .\samples\imgs\a2-sphere.png

cis4800a2 -camera a1 -if .\samples\xobjs\torus.xobj -of .\samples\imgs\a1-torus.png
cis4800a2 -camera a2 -if .\samples\xobjs\torus.xobj -of .\samples\imgs\a2-torus.png

cis4800a2 -camera a1 -if .\samples\xobjs\tube.xobj -of .\samples\imgs\a1-tube.png
cis4800a2 -camera a2 -if .\samples\xobjs\tube.xobj -of .\samples\imgs\a2-tube.png

cis4800a2 -camera cam1 -if .\samples\xobjs\car.xobj -of .\samples\imgs\a2-car-1.png
cis4800a2 -camera cam2 -if .\samples\xobjs\car.xobj -of .\samples\imgs\a2-car-2.png
