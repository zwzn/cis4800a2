package le

import (
	"fmt"
	"math"
)

type Vector2 struct {
	X float64
	Y float64
}

var Zero2 *Vector2 = &Vector2{0, 0}

func Vec2(x float64, y float64) *Vector2 {
	return &Vector2{x, y}
}

func (v *Vector2) Clone() *Vector2 {
	return &Vector2{
		v.X,
		v.Y,
	}
}

func (v1 *Vector2) Add(v2 *Vector2) *Vector2 {
	return &Vector2{
		v1.X + v2.X,
		v1.Y + v2.Y,
	}
}
func (v1 *Vector2) Subtract(v2 *Vector2) *Vector2 {
	return &Vector2{
		v1.X - v2.X,
		v1.Y - v2.Y,
	}
}

func (v *Vector2) Dot(a float64) *Vector2 {
	return &Vector2{
		a * v.X,
		a * v.Y,
	}
}

func (this *Vector2) Distance(v *Vector2) float64 {
	return math.Sqrt(math.Pow(this.X-v.X, 2) + math.Pow(this.Y-v.Y, 2))
}

func (v *Vector2) String() string {
	return fmt.Sprintf("(%.2f, %.2f)", v.X, v.Y)
}
