package object

import "math"

func NewCylinder(sections int) *Mesh {
	// TODO split vertically into more triangles
	var m *Mesh
	m = newMesh()
	dps := math.Pi * 2 / float64(sections)

	nTop := Vec3(0, 1, 0)
	nBottom := Vec3(0, -1, 0)

	for angle := 0.0; angle < math.Pi*2; angle += dps {
		var pTop, pBottom, p1, p2, p3, p4 *Vector3

		pTop = Vec3(0, 1, 0)
		pBottom = Vec3(0, -1, 0)

		p1 = angleToPoint(angle, 1).Add(Vec3(0, 1, 0))
		p2 = angleToPoint(angle+dps, 1).Add(Vec3(0, 1, 0))
		p3 = angleToPoint(angle, 1).Add(Vec3(0, -1, 0))
		p4 = angleToPoint(angle+dps, 1).Add(Vec3(0, -1, 0))

		n1 := NewDirectionVector3(angle, 0)
		n2 := NewDirectionVector3(angle+dps, 0)

		m.addTriangle(NewTriangle(pTop, p1, p2, nTop, nTop, nTop))
		m.addTriangle(NewTriangle(pBottom, p3, p4, nBottom, nBottom, nBottom))

		pps := 2 / float64(sections)
		for i := -1.0; i < 1; i += pps {
			p1 = angleToPoint(angle, 1).Add(Vec3(0, i, 0))
			p2 = angleToPoint(angle+dps, 1).Add(Vec3(0, i+pps, 0))
			p3 = angleToPoint(angle, 1).Add(Vec3(0, i+pps, 0))
			p4 = angleToPoint(angle+dps, 1).Add(Vec3(0, i, 0))

			m.addTriangle(NewTriangle(p1, p2, p3, n1, n2, n1))
			m.addTriangle(NewTriangle(p2, p3, p4, n2, n1, n2))
		}
	}
	return m
}

func angleToPoint(angle, distance float64) *Vector3 {
	return Vec3(
		math.Cos(angle)*distance,
		0,
		math.Sin(angle)*distance,
	)
}
