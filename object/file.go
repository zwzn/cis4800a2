package object

import (
	"encoding/xml"
	"image/color"
	"io/ioutil"
	"math"
	"strings"

	"bitbucket.org/zwzn/cis4800a2/matrix"

	colors "gopkg.in/go-playground/colors.v1"
)

func FromFile(file string) (*Object, error) {
	type T struct {
		Type string  `xml:"type,attr"`
		X    float64 `xml:"x,attr"`
		Y    float64 `xml:"y,attr"`
		Z    float64 `xml:"z,attr"`
	}
	type A struct {
		Type  string  `xml:"type,attr"`
		Pitch float64 `xml:"pitch,attr"`
		Yaw   float64 `xml:"yaw,attr"`
		Roll  float64 `xml:"roll,attr"`
	}
	type Sph struct {
		Pitch    float64 `xml:"pitch,attr"`
		Yaw      float64 `xml:"yaw,attr"`
		Distance float64 `xml:"distance,attr"`
	}
	type C struct {
		Name       string  `xml:"name,attr"`
		Position   T       `xml:"position"`
		Angle      A       `xml:"angle"`
		Spherical  Sph     `xml:"spherical"`
		FOV        float64 `xml:"fov,attr"`
		Projection string  `xml:"projection,attr"`
	}
	type M struct {
		Shape           string  `xml:"shape,attr"`
		Faces           int     `xml:"resolution,attr"`
		Hole            float64 `xml:"hole,attr"`
		Transformations []T     `xml:"transform"`
		Colour          string  `xml:"colour,attr"`
	}
	type O struct {
		Meshes  []M `xml:"mesh"`
		Cameras []C `xml:"camera"`
	}
	v := O{}
	dat, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	// fmt.Println(string(dat))
	err = xml.Unmarshal(dat, &v)
	if err != nil {
		return nil, err
	}

	obj := NewObject()

	for _, cam := range v.Cameras {
		var c *Camera
		dtor := math.Pi * 2 / 360
		fov := cam.FOV * dtor
		if fov <= 0 || fov >= 180*dtor {
			fov = 45 * dtor
		}

		pro := PERSPECTIVE

		if strings.EqualFold(cam.Projection, "perspective") {
			pro = PERSPECTIVE
		} else if strings.EqualFold(cam.Projection, "parallel") {
			pro = PARALLEL
		}

		if cam.Spherical.Distance == 0 && cam.Spherical.Pitch == 0 && cam.Spherical.Yaw == 0 {
			c = NewCamera(
				Vec3(cam.Position.X, cam.Position.Y, cam.Position.Z),
				Vec3(cam.Angle.Pitch*dtor, cam.Angle.Yaw*dtor, cam.Angle.Roll*dtor),
				fov, cam.Name, pro)
			// fmt.Printf("\nMatrix:\n%v\n\n", c.GetTransformations)
		} else {
			m := matrix.Identity(4)

			m, err = matrix.Translate(0, 0, -cam.Spherical.Distance).Multiply(m)
			if err != nil {
				return nil, err
			}
			m, err = matrix.Rotate(cam.Spherical.Pitch*dtor, 0, 0).Multiply(m)
			if err != nil {
				return nil, err
			}
			m, err = matrix.Rotate(0, cam.Spherical.Yaw*dtor, 0).Multiply(m)
			if err != nil {
				return nil, err
			}

			pos, err := Zero3.Transform(m)
			if err != nil {
				return nil, err
			}
			dir := Vec3(cam.Spherical.Pitch*dtor, cam.Spherical.Yaw*dtor, 0)
			c = NewCamera(pos, dir, fov, cam.Name, pro)
			// fmt.Println(pos, dir)
		}

		obj.AddCamera(c)

	}

	for _, mesh := range v.Meshes {
		m := newMesh()
		if mesh.Shape == "sphere" {
			m = NewSphere(mesh.Faces)
		} else if mesh.Shape == "cube" {
			m = NewCube(mesh.Faces)
		} else if mesh.Shape == "cylinder" {
			m = NewCylinder(mesh.Faces)
		} else if mesh.Shape == "tube" {
			m = NewTube(mesh.Faces, mesh.Hole)
		} else if mesh.Shape == "torus" {
			m = NewTorus(mesh.Faces, mesh.Hole)
		}
		// fmt.Printf("%v\n", mesh.Transformations)
		for _, tr := range mesh.Transformations {
			if tr.Type == "translate" {
				err := m.Transform(matrix.Translate(tr.X, tr.Y, tr.Z))
				if err != nil {
					return nil, err
				}
			} else if tr.Type == "scale" {
				err := m.Transform(matrix.Scale(tr.X, tr.Y, tr.Z))
				if err != nil {
					return nil, err
				}
			} else if tr.Type == "rotate" {
				dtor := math.Pi * 2 / 360
				err := m.Transform(matrix.Rotate(tr.X*dtor, tr.Y*dtor, tr.Z*dtor))
				if err != nil {
					return nil, err
				}
			}
		}

		if mesh.Colour == "" {
			m.SetColour(color.RGBA{0, 0, 0, 255})
		} else {
			hex, err := colors.ParseHEX(mesh.Colour)
			if err != nil {
				return nil, err
			}

			rgba := hex.ToRGBA()
			m.SetColour(color.RGBA{uint8(rgba.R), uint8(rgba.G), uint8(rgba.B), uint8(rgba.A * 255)})
		}
		obj.AddMesh(m)
	}
	return obj, nil
}
