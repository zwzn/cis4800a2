package object

type Object struct {
	meshes  []*Mesh
	cameras []*Camera
}

func NewObject() *Object {
	return &Object{}
}

func (o *Object) GetFaces() []*Triangle {
	var faces []*Triangle
	for _, mesh := range o.meshes {
		faces = append(faces, mesh.GetFaces()...)
	}
	return faces
}

func (o *Object) AddMesh(m *Mesh) {
	o.meshes = append(o.meshes, m)
}

func (o *Object) AddCamera(c *Camera) {
	o.cameras = append(o.cameras, c)
}

func (o *Object) GetCamera(name string) *Camera {
	if len(o.cameras) <= 0 {
		return NewCamera(Vec3(0, 0, -1), Vec3(0, 0, 0), 45, "", PERSPECTIVE)
	}

	for _, cam := range o.cameras {
		if cam.name == name {
			return cam
		}
	}

	return o.cameras[0]
}
