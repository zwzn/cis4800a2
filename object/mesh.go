package object

import (
	"image/color"

	"bitbucket.org/zwzn/cis4800a2/matrix"
)

type Mesh struct {
	faces          []*Triangle
	Transformation *matrix.Matrix
}

type Triangle struct {
	A       *Vector3
	B       *Vector3
	C       *Vector3
	NormalA *Vector3
	NormalB *Vector3
	NormalC *Vector3
	Colour  color.RGBA
	Mesh    *Mesh
}

func newMesh() *Mesh {
	m := &Mesh{}
	m.Transformation = matrix.Identity(4)
	return m
}

func (m *Mesh) addTriangle(t *Triangle) {
	t.Mesh = m
	m.faces = append(m.faces, t)
}

func (m *Mesh) GetFaces() []*Triangle {
	return m.faces
}

func (this *Mesh) SetColour(c color.RGBA) {
	for _, face := range this.GetFaces() {
		face.Colour = c
	}
}
func NewTriangle(a, b, c, na, nb, nc *Vector3) *Triangle {
	return &Triangle{a, b, c, na, nb, nc, color.RGBA{0, 0, 0, 0}, nil}
}

func (t *Triangle) Normal(v *Vector3) *Vector3 {
	// v := t.B.Subtract(t.A)
	// w := t.C.Subtract(t.A)

	// norm := NewVector3(
	// 	(v.Y*w.Z)-(v.Z*w.Y),
	// 	(v.Z*w.X)-(v.X*w.Z),
	// 	(v.X*w.Y)-(v.Y*w.X))
	dv := Vec3(
		v.Distance(t.A),
		v.Distance(t.B),
		v.Distance(t.C))

	// fmt.Println(v)

	return Zero3.
		Add(t.NormalA.Scalar(dv.X)).
		Add(t.NormalB.Scalar(dv.Y)).
		Add(t.NormalC.Scalar(dv.Z)).ToUnit()
}

func (t *Triangle) NormalFlat() *Vector3 {
	v := t.B.Subtract(t.A)
	w := t.C.Subtract(t.A)

	return Vec3(
		(v.Y*w.Z)-(v.Z*w.Y),
		(v.Z*w.X)-(v.X*w.Z),
		(v.X*w.Y)-(v.Y*w.X)).ToUnit()
}

func (t *Triangle) Distance(v *Vector3) float64 {
	return (t.A.Distance(v) + t.B.Distance(v) + t.C.Distance(v)) / 3
}
