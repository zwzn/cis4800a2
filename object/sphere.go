package object

import (
	"math"
)

func NewSphere(sections int) *Mesh {
	var m *Mesh
	m = newMesh()
	dps := math.Pi * 2 / float64(sections)
	for lat := -math.Pi / 2; lat < math.Pi/2; lat += dps {
		for long := 0.0; long < math.Pi*2; long += dps {
			var p1, p2, p3, p4, n1, n2, n3, n4 *Vector3
			p1 = longLatToPoint(long, lat)
			p2 = longLatToPoint(long, lat+dps)
			p3 = longLatToPoint(long+dps, lat)
			p4 = longLatToPoint(long+dps, lat+dps)

			n1 = NewDirectionVector3(long, lat)
			n2 = NewDirectionVector3(long, lat+dps)
			n3 = NewDirectionVector3(long+dps, lat)
			n4 = NewDirectionVector3(long+dps, lat+dps)

			m.addTriangle(NewTriangle(p1, p2, p3, n1, n2, n3))
			m.addTriangle(NewTriangle(p2, p3, p4, n2, n3, n4))
		}
	}
	return m
}

func longLatToPoint(long float64, lat float64) *Vector3 {
	var rad float64
	rad = math.Cos(lat)
	return Vec3(
		rad*math.Cos(long),
		math.Sin(lat),
		rad*math.Sin(long),
	)
}
