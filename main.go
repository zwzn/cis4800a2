package main

import (
	"flag"
	"image"
	"image/png"
	"log"
	"os"

	"bitbucket.org/zwzn/cis4800a2/object"
	"bitbucket.org/zwzn/cis4800a2/renderer"
)

func check(e error) {
	if e != nil {
		log.Fatalf("%v\n", e)
	}
}

func main() {
	// pl := le.NewPlaneFromPoints(le.Zero3, le.Vec3(1, 0, 0), le.Vec3(0, 1, 0))
	// li := le.NewLineFromPoints(le.Vec3(0, 0, 1), le.Vec3(1, 1, 0.5))
	// fmt.Println(pl)
	// fmt.Println(li)
	// // fmt.Println(pl)
	// po := pl.Intersection(li)

	// fmt.Println(po)

	// return

	inFile := flag.String("if", "", "an xobj file to be rendered")
	outFile := flag.String("of", "image.png", "the image file to output")
	rend := flag.String("renderer", "a2", "The renderer to use")
	size := flag.Int("size", 1000, "The size of the output image")
	camera := flag.String("camera", "", "The name of the camera to use of empty for default")
	flag.Parse()

	// v := object.NewVector3(1, 2, 3)
	// fmt.Println(v)
	// fmt.Println(v.ToBasis(object.IdentityBasis))

	if *inFile == "" {
		log.Fatalf("You must enter an xobj file to be rendered\n")
	}
	// num := int(math.Sqrt(1000) / 2)
	// o := object.NewCylinder(50)

	obj, err := object.FromFile(*inFile)
	check(err)
	var img image.Image
	if *rend == "adam" {
		img, err = renderer.RenderObject2(obj, *size, *size)
		check(err)
	} else if *rend == "a2" {
		img, err = renderer.RenderObject(obj, *size, *size, *camera)
		check(err)
	} else {
		img, err = renderer.RenderObject(obj, *size, *size, *camera)
		check(err)
	}
	saveImage(img, *outFile)
}

func saveImage(img image.Image, file string) {

	f, err := os.Create(file)
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
