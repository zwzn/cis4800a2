package matrix

import (
	"math"
)

func Translate(x, y, z float64) *Matrix {
	return NewMatrix(4, 4,
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		x, y, z, 1,
	)
}

func Scale(x, y, z float64) *Matrix {
	return NewMatrix(4, 4,
		x, 0, 0, 0,
		0, y, 0, 0,
		0, 0, z, 0,
		0, 0, 0, 1,
	)
}

func RotateX(a float64) *Matrix {
	return NewMatrix(4, 4,
		1, 0, 0, 0,
		0, math.Cos(a), -math.Sin(a), 0,
		0, math.Sin(a), math.Cos(a), 0,
		0, 0, 0, 1,
	)
}
func RotateY(a float64) *Matrix {
	return NewMatrix(4, 4,
		math.Cos(a), 0, math.Sin(a), 0,
		0, 1, 0, 0,
		-math.Sin(a), 0, math.Cos(a), 0,
		0, 0, 0, 1,
	)
}
func RotateZ(a float64) *Matrix {
	return NewMatrix(4, 4,
		math.Cos(a), -math.Sin(a), 0, 0,
		math.Sin(a), math.Cos(a), 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1,
	)
}

func Rotate(pitch, yaw, roll float64) *Matrix {
	m, _ := RotateZ(roll).Multiply(RotateX(pitch))
	m, _ = m.Multiply(RotateY(yaw))
	return m

}
